# DEPRECATED

This git repository has been deprecated and is kept for past reference only.

`respdiff` tool has been moved to https://gitlab.labs.nic.cz/knot/respdiff

`cache_usage_benchmark` has been deprecated.
