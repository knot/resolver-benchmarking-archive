'''
Created on Oct 24, 2016

@author: Jan Holusa
'''

import os
import writer
import logging
import time

log = logging.getLogger()


class resperf(object):
    '''
    Maintain resperf program
    '''

    def __init__(self, parameters, resolvers_list):
        '''
        Constructor

        Args:
            parameters: instance of params class
            resolvers_list (array): array of resolver instances
        '''
        self.tested_resolvers = resolvers_list
        self.parameters = parameters

        # default params for A test
        self.rA, self.cA, self.mA, self.CA = self.parameters.parse_params(
            self.parameters.pre_resperf_param)

        # default params for B test
        self.rB, self.cB, self.mB, self.CB = self.parameters.parse_params(
            self.parameters.resperf_param)

    def run_test(self, ips, infile, port, test, resolver, percent):
        '''
        Start particular test. Test is making output into file params.data.

        Args:
            ips (string): ip of running resolver.
            infile (str): path to the file to be tested.
            port (str): port of the runnung resolver.
            test (A|B): string "A" or "B" meaning A(=filling up cache) test
                        or B(=measuring) test.
            resolver (string): name of the tested resolver
            percent (string): percentage of filled cache which is currently testing.
        '''
        if test == 'A':
            log.debug("Running test %s: %s" % (test, infile))
            os.system("resperf -s %s -d %s %s -p %s >%s 2>/dev/null" % (
                ips, infile, self.parameters.pre_resperf_param, port, self.parameters.data))
        if test == 'B':
            log.info(" Running test %s: %s" % (test, infile))
            os.system("resperf -P %s -s %s -d %s %s -p %s >%s" % (
                os.path.join(
                    self.parameters.path_results, resolver + '_' + str(percent) + '.gnuplot'),
                ips,
                infile,
                self.parameters.resperf_param,
                port,
                self.parameters.data))

            print("bash %s %s" % (os.path.join(self.parameters.curdir, "create_plot.sh"),
                                  os.path.join(self.parameters.path_results,
                                               resolver + "_" + str(percent))))
            os.system("bash %s %s" % (os.path.join(self.parameters.curdir, "create_plot.sh"),
                                      os.path.join(self.parameters.path_results,
                                                   resolver + '_' + str(percent))))

    def start(self, one_percentage_lines):
        '''
        Start test in general.
        See: run_test()

        Args:
            one_percentage_lines (int): how many lines of input file is equal to one percentage.
        '''
        my_writer = writer.writer(self.parameters.result_file,
                                  self.parameters.out_js,
                                  self.parameters.timestemp,
                                  len(self.parameters.percentage_array),
                                  len(self.parameters.resolver_ip))

        my_writer.set_count_of_clients(self.CB)
        my_writer.set_branch(self.parameters.knot_branch)
        # Main loop
        for active_resolver in self.tested_resolvers:
            # FIXME: nebere jmeno a unbound nefunguje jak ma
            my_writer.set_resolver_name(active_resolver.name)

            for x in range(0, len(self.parameters.percentage_array)):
                my_writer.set_cache_usage_percent(self.parameters.percentage_array[x])

                start_iteration = time.time()
                log.info("-----------------------------")
                log.info("> Iteration: %s out of %s." %
                         (str(x), str(len(self.parameters.percentage_array) - 1)))
                print("> Iteration: %s out of %s." %
                      (str(x), str(len(self.parameters.percentage_array) - 1)))
                QPS_ARR = []
                SENDED_ARR = []
                LOST_ARR = []

                for iter in range(0, self.parameters.iteration):
                    y = 0  # Server crash check
                    # Loop check that measuring was without server crash
                    while True:
                        y += 1
                        # Fill up part below vv
                        # For 0% skip filling part
                        if x != 0:
                            usedfile = os.path.join(
                                self.parameters.workdir,
                                'queryfile_' + str(self.parameters.percentage_array[x])
                            )
                            log.debug(usedfile)
                            attemp_lost = 100
                            attemp_cnt = 0
                            attemp_send = 0
                            # threshold = 0 if active_resolver.name != "bind" else 70
                            threshold = 50
                            warm_up = 0  # first number of run even lost is 0
                            run_cnt = 0  # how many times it runned
                            cache_need = one_percentage_lines * int(
                                self.parameters.percentage_array[x])
                            while (attemp_lost > threshold) or \
                                    (run_cnt <= warm_up) or (attemp_send < cache_need):
                                self.run_test(
                                    active_resolver.ip, usedfile, active_resolver.port, "A",
                                    active_resolver.name, int(
                                        self.parameters.percentage_array[x]))
                                sended, qps, lost, rrcode = self.parse_resperf_output()
                                attemp_send = int(sended)
                                attemp_lost = int(lost)
                                attemp_cnt += 1

                                if (attemp_cnt % 5) == 0 and (run_cnt > warm_up):
                                    threshold += 5
                                    log.debug("> Threshold increased to: %d percent" % threshold)
                                run_cnt += 1

                            log.info(
                                "> Used from test A[lost|QPS|sended]: [%s|%s|%s]" %
                                (str(
                                    round(lost)), str(
                                    round(qps)), str(
                                    round(sended))))
                            log.info("> Filling up cache: %d times" % run_cnt)
                        # Fill up cache part above ^^

                        big_file = os.path.join(self.parameters.workdir, 'queryfile_100')
                        self.run_test(
                            active_resolver.ip, big_file, active_resolver.port, "B",
                            active_resolver.name, int(
                                self.parameters.percentage_array[x]))

                        sended, qps, lost, rrcode = self.parse_resperf_output()
                        if qps == 0:  # = Server crash
                            y -= 1
                            log.warning(">STH WRONG -> Repeate iteration!")
                            active_resolver.cache_clear()
                            continue

                        my_writer.add_values(qps, sended, lost, rrcode)

                        QPS_ARR.append(qps)
                        SENDED_ARR.append(sended)
                        LOST_ARR.append(lost)

                        active_resolver.cache_clear()

                        if y == 1:
                            break
                            # end while True:
                # end for iter in range(0, ITERATION):

                # count avg QPS
                log.debug(QPS_ARR)
                qps = sum(QPS_ARR) / len(QPS_ARR)
                sended = sum(SENDED_ARR) / len(SENDED_ARR)
                lost = sum(LOST_ARR) / len(LOST_ARR)
                log.info("Cache is " + str(self.parameters.percentage_array[x]) + "% full.")
                log.info("> Lost:" + str(lost) + "|QPS: " + str(qps) + "|Sended: " + str(sended))
                #                 output(f, sended, qps, lost, float(x/10.0), rrcode, timestemp)

                time_iteration = time.time() - start_iteration
                m, s = divmod(time_iteration, 60)
                h, m = divmod(m, 60)
                print("> Iteration time: %d:%02d:%02d" % (h, m, s))
                log.info("> Iteration time: %d:%02d:%02d" % (h, m, s))
                my_writer.write_out()

                # end for x in range (0,11):
        # end while ACTIVE_RESOLVER < len(RESOLVER)
        log.info("Resperf start done :)")

    def parse_resperf_output(self):
        '''
        Parse output of resperf and return runtime, throughput, lost
            Retval: (sended_querries(float), QPS(float), lost_querries(float), response_codes(str))
        '''
        log.debug("Parsing")
        # parse & prepare output
        with open(self.parameters.data, "r") as myfile:
            # truncate
            string = myfile.read()
            data_start = string.find("Statistics:")
            useful_data = string[data_start:]

            # get usefull data
            # +q_send
            q_send_str = "Queries sent:"
            q_send_position_start = useful_data.find(q_send_str)
            q_send_position_end = useful_data.find("\n", q_send_position_start)
            q_send = useful_data[q_send_position_start + len(q_send_str):q_send_position_end]
            q_send = q_send.strip()

            # +q_send
            resp_codes_str = "Response codes:"
            resp_codes_position_start = useful_data.find(resp_codes_str)
            resp_codes_position_end = useful_data.find("\n", resp_codes_position_start)
            resp_codes = useful_data[resp_codes_position_start +
                                     len(resp_codes_str):resp_codes_position_end]
            resp_codes = resp_codes.strip()
            # resp_codes = dict(item.strip().split(" ", 1) for item in resp_codes.split(','))

            # +qps
            qps_str = "Maximum throughput:"
            qps_position_start = useful_data.find(qps_str)
            qps_position_end = useful_data.find("\n", qps_position_start + len(qps_str))
            qps = useful_data[qps_position_start + len(qps_str):qps_position_end - 3]
            qps = qps.strip()

            # +lost
            lost_str = "Lost at that point:"
            lost_position_start = useful_data.find(lost_str)
            lost_position_end = useful_data.find("%", lost_position_start + len(lost_str))
            lost = useful_data[lost_position_start + len(lost_str):lost_position_end - 1]
            lost = lost.strip()

        if not q_send:
            q_send = "0"
        if not qps:
            qps = "0"
        if not lost:
            lost = "100"
        log.debug("sended: " + q_send + "| qps: " + qps + "| lost: " + lost)

        return float(q_send), float(qps), float(lost), resp_codes
