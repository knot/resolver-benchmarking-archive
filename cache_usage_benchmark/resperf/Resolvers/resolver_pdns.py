'''
Created on Nov 7, 2016

@author: Jan Holusa
'''

import os
import logging
import subprocess
from resolver import resolver

log = logging.getLogger()


class resolverPdns(resolver):
    '''
    Maintain Power DNS resolver
    '''

    def remote_start(self):
        if self.remote_start_flag:
            super(resolverPdns, self).remote_start()
            command = "bash %s" % (os.path.join(self.tmp, self.name + ".sh"))
            self.remote_command("ssh", self.ip, command)

        self.check_server()

    def cache_clear(self):
        with open(os.devnull, 'w') as FNULL:
            log.debug("PDNS: rec_control wipe-cache")
            p1 = subprocess.Popen(["ssh", self.ip, "rec_control wipe-cache $"],
                                  stdout=FNULL, stderr=FNULL)
            p1.communicate()[0]
            if p1.returncode == 1:
                raise RuntimeError("Cannot wipe Pdns cache.")
