'''
Created on Sep 23, 2016

@author: Jan Holusa
'''
import os
import logging
import subprocess
from resolver import resolver

log = logging.getLogger()


class resolverUnbound(resolver):
    '''
    Maintain Unbound resolver
    '''

    def remote_start(self):
        if self.remote_start_flag:
            super(resolverUnbound, self).remote_start()
            command = "bash %s" % (os.path.join(self.tmp, self.name + ".sh"))
            self.remote_command("ssh", self.ip, command)

        self.check_server()

    def cache_clear(self):
        with open(os.devnull, 'w') as FNULL:
            log.debug("Unbound: unbound-control reload")
            p1 = subprocess.Popen(["unbound-control", "-s", self.ip,
                                   "reload"], stdout=FNULL, stderr=FNULL)
            p1.communicate()[0]
            if p1.returncode == 1:
                raise RuntimeError("Cannot flush Unbound cache.")
