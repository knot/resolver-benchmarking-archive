'''
Created on Sep 14, 2016

@author: Jan Holusa
'''

import subprocess
import os
import logging
from resolver import resolver

log = logging.getLogger()


class resolverKnot(resolver):
    '''
    Maintain Knot resolver
    '''

    def __init__(self, ip, port, name, script_folder, resolver_config_folder, branch, pathWorkdir,
                 local_interface_name, service_port='50004'):
        super(resolverKnot, self).__init__(ip, port, name, script_folder, resolver_config_folder,
                                           local_interface_name)
        self.path = pathWorkdir
        self.socket_number = None
        self.service_port = service_port
        self.branch = branch
        self.commit = "??"

    def remote_start(self):
        if self.remote_start_flag:
            super(resolverKnot, self).remote_start()
            self.kill_remote_service(self.ip, "0.0.0.0", self.service_port)
            command = "bash %s %s %s" % (os.path.join(
                self.tmp, self.name + ".sh"), self.branch, self.service_port)
            rv = self.remote_command("ssh", self.ip, command)
            self.commit = rv.strip()
            log.info("Knot Branch, commit: %s, %s" % (self.branch, self.commit))

        self.check_server()

    def cache_clear(self):
        with open(os.devnull, 'w') as FNULL:
            log.debug("KNOT: Remote cache.clear()")
            p1 = subprocess.Popen(["echo", "cache.clear()"], stdout=subprocess.PIPE)
            p2 = subprocess.Popen(["nc", self.ip, str(self.service_port)],
                                  stdin=p1.stdout, stdout=FNULL, stderr=FNULL)
            p2.communicate()[0]
            if p2.returncode == 1:
                raise RuntimeError("Cannot flush Kresd cache.")

    def stop_server(self):
        '''
        Kill actual service (=resolver + socat)
        '''
        # kill resolver
        self.kill_remote_service(self.ip, self.ip, self.port)
        # kill socat
        self.kill_remote_service(self.ip, '0.0.0.0', self.service_port)
