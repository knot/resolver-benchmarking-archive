'''
Created on Sep 20, 2016

@author: Jan Holusa
'''

import os
import logging
import subprocess
from resolver import resolver

log = logging.getLogger()


class resolverBind(resolver):
    '''
    Maintain bind resolver
    '''

    def cache_clear(self):
        with open(os.devnull, 'w') as FNULL:
            log.debug("BIND: rndc flush")
            p1 = subprocess.Popen(["rndc", "-s", self.ip, "flush"], stdout=FNULL, stderr=FNULL)
            p1.communicate()[0]
            if p1.returncode == 1:
                raise RuntimeError("Cannot flush Bind cache.")

    def remote_start(self):
        if self.remote_start_flag:
            super(resolverBind, self).remote_start()
            command = "bash %s" % (os.path.join(self.tmp, self.name + ".sh"))
            self.remote_command("ssh", self.ip, command)

        self.check_server()
