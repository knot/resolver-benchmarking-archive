'''
Created on Mar 21, 2017

@author: root
'''
from . import resolver_bind
from . import resolver_knot
from . import resolver_unbound
from . import resolver_pdns
from . import resolver

__all__ = ['resolver_bind', 'resolver_knot', 'resolver_unbound',
           'resolver_pdns', 'resolver']
