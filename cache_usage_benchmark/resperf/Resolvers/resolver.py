'''
Created on Mar 21, 2017

@author: Jan Holusa
'''

import logging
import os
import socket
import subprocess
import fcntl
import struct

log = logging.getLogger()


class resolver(object):
    '''
    Parent class for all resolvers.
    '''

    def __init__(self, ip, port, name, script_folder, resolver_config_folder, local_interface_name):
        log.info("Start %s: %s:%s" % (name, ip, port))
        self.ip = ip
        self.port = port
        self.name = name
        self.script_folder = script_folder
        self.resolver_config_folder = resolver_config_folder
        self.tmp = "/tmp/kresdbench/"  # directory at the remote server
        self.remote_start_flag = False
        self.local_interface_name = local_interface_name
        self.local_ip = self.get_ip_address(self.local_interface_name)

    def cache_clear(self):
        '''
        Clear the cache of particular resolver.
        '''
        raise NotImplementedError("Subclass must implement abstract method")

    def remote_start(self):
        '''
        If start flag is true - copy and run starting resolver script to
        remote server.
        Also try remote server:port if is it open.
        '''
        if self.remote_start_flag:
            # Send config folder
            log.info("Copying %s script + config to %s" % (self.name, self.ip))
            command = "rsync -r %s %s:%s" % (os.path.join(self.resolver_config_folder, self.name),
                                             self.ip, self.tmp)
            log.info(command)
            self.remote_copy("rsync",
                             os.path.join(self.resolver_config_folder, self.name + "-conf"),
                             self.ip + ":" + self.tmp, "-r")

            # Send scripts (cleaner and resolver starter)
            self.remote_copy(program="rsync",
                             from_location=os.path.join(self.script_folder, "cleaner.sh"),
                             to_location=self.ip + ":" + self.tmp)
            self.remote_copy(program="rsync",
                             from_location=os.path.join(self.script_folder, self.name + ".sh"),
                             to_location=self.ip + ":" + self.tmp)

            log.info("Generate config %s" % self.name)
            self.generate_resolver_config()

            log.info("Starting %s" % self.name)
            # First make sure, that there is no resolver running at needed port
            self.kill_remote_service(self.ip, self.ip, self.port)

    def stop_server(self):
        '''
        Kill actual service (=resolver)
        '''
        self.kill_remote_service(self.ip, self.ip, self.port)

    def kill_remote_service(self, remote_server, run_adr, run_port):
        '''
        Kill particular service (=resolver) using cleaner.sh
        '''
        log.info("sudo bash %s %s %s" % (os.path.join(self.tmp, "cleaner.sh"), run_adr, run_port))
        command = "sudo bash %s %s %s" % (os.path.join(self.tmp, "cleaner.sh"), run_adr, run_port)
        self.remote_command("ssh", remote_server, command)

    def remote_start_set(self, value):
        self.remote_start_flag = value

    def generate_resolver_config(self):
        log.info("Generating %s config" % self.name)
        command = "bash %s %s %s %s" % (
            os.path.join(self.tmp, self.name + "-conf", self.name + "-conf-generator.sh"),
            self.ip, self.port, self.local_ip)
        self.remote_command("ssh", self.ip, command)

    def check_server(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex((self.ip, int(self.port)))
        if result == 0:
            log.debug("Address:  " + self.ip + ":" + str(self.port) + " is open.")
        else:
            log.error("Address:  " + self.ip + ":" + str(self.port) + " is NOT open.")
            raise IOError("Address:  " + self.ip + ":" + str(self.port) + " is NOT open.")

    @staticmethod
    def remote_command(program, ip, command):
        log.info("%s %s %s" % (program, ip, command))
        ssh = subprocess.Popen(
            [program, ip, command], shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ssh.wait()
        retcode = ssh.returncode
        if retcode == 1:
            raise IOError("Cannot execute: %s." % command)

        try:
            retval = ssh.stdout.readlines()[0]
        except IndexError:
            raise IOError("Connection failed: %s" % command)
        log.debug("Command done.")
        return retval

    @staticmethod
    def remote_copy(program, from_location, to_location, param=None):
        if not param:
            param = ""
        log.info("%s %s %s %s", program, param, from_location, to_location)
        with open(os.devnull, 'w') as FNULL:
            ssh = subprocess.Popen(
                [program, param, from_location, to_location], shell=False, stdout=FNULL,
                stderr=FNULL)
            ssh.wait()
            ssh.communicate()[0]
            if ssh.returncode == 1:
                raise IOError(
                    "Cannot execute: %s %s %s %s." %
                    (program, param, from_location, to_location))
        log.debug("Copy done.")

    @staticmethod
    def get_ip_address(ifname):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        return socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', ifname[:15])
        )[20:24])
