'''
Created on Feb 3, 2017

@author: Jan Holusa
'''
import signal
import params
import logging
import time
import sys
import os
import local_constants as C
import Resolvers
import resperf
import input_file_splitter

log = logging.getLogger()


class handler(object):
    '''
    Main program handler.
    '''

    def __init__(self):
        '''
        Main program handler class creator.
        '''
        # Instance of parameters class
        self.parameters = None
        # Array of instances Resolver class
        self.resolvers = []
        # Starting time of benchmark
        self.start_time = None

    def signal_handler(self, signal, frame):
        log.warning("Signal caught!")
        print("Signal caught!")
        self.finalize()

    def start(self):
        '''
        Method is creating instance of measurement class.
        This class is also wrapping benchmark itself.
        '''
        self.start_time = time.time()
        # logging Setup
        form = '%(asctime)-15s %(levelname)s - %(message)s'
        logging.basicConfig(level=logging.DEBUG, filename=C.LOG, format=form)
        self.parameters = params.params(self.start_time)
        # logging Setup
        logging.basicConfig(level=logging.DEBUG, filename=self.parameters.logfile, format=form)

        # register signals
        signal.signal(signal.SIGINT, self.signal_handler)
        signal.signal(signal.SIGTERM, self.signal_handler)

        # Read config files
        self.parameters.read_params()

        # Start resolvers
        self.start_resolvers()

        self.clear_caches()

        # Prepare input files
        # Create input files for resperf
        file_creator = input_file_splitter.fileSplitter(self.parameters)
        # Estimate how big has to be small and big file:
        rA, cA, mA, CA = self.parameters.parse_params(self.parameters.pre_resperf_param)
        rB, cB, mB, CB = self.parameters.parse_params(self.parameters.resperf_param)
        file_creator.estimate_size(rA, cA, mA, False)
        file_creator.estimate_size(rB, cB, mB, True)
        # split input file
        try:
            file_creator.splitFile()
        except (TypeError, IOError) as detail:
            log.error("Error: %s" % detail)
            sys.stderr.write("Error: %s" % detail)
            sys.exit(1)

        rp = resperf.resperf(self.parameters, self.resolvers)
        rp.start(file_creator.one_percentage)

        self.finalize()

    def clear_caches(self):
        '''
        Clear all caches of resolver instances which are used.
        '''
        for resolver in self.resolvers:
            resolver.cache_clear()

    def start_resolvers(self):
        '''
        Create instances describing particular resolvers.
        Also remote_start() is called for starting resolvers remotely.
        '''
        resolver_object = None
        for name, ip in self.parameters.resolver_ip.items():
            if name == "knot":
                resolver_object = Resolvers.resolver_knot.resolverKnot(
                    ip=ip, port=self.parameters.resolver_port[name], name=name,
                    script_folder=C.SCRIPT_FOLDER, resolver_config_folder=C.RESOLVER_CONFIG_FOLDER,
                    branch=self.parameters.knot_branch, pathWorkdir=self.parameters.workdir,
                    local_interface_name=self.parameters.local_interface,
                    service_port=self.parameters.knot_service_port)
            elif name == "bind":
                resolver_object = Resolvers.resolver_bind.resolverBind(
                    ip=ip, port=self.parameters.resolver_port[name], name=name,
                    script_folder=C.SCRIPT_FOLDER, resolver_config_folder=C.RESOLVER_CONFIG_FOLDER,
                    local_interface_name=self.parameters.local_interface)
            elif name == "unbound":
                resolver_object = Resolvers.resolver_unbound.resolverUnbound(
                    ip=ip, port=self.parameters.resolver_port[name], name=name,
                    script_folder=C.SCRIPT_FOLDER, resolver_config_folder=C.RESOLVER_CONFIG_FOLDER,
                    local_interface_name=self.parameters.local_interface)
            elif name == "pdns":
                resolver_object = Resolvers.resolver_pdns.resolverPdns(
                    ip=ip, port=self.parameters.resolver_port[name], name=name,
                    script_folder=C.SCRIPT_FOLDER, resolver_config_folder=C.RESOLVER_CONFIG_FOLDER,
                    local_interface_name=self.parameters.local_interface)
            else:
                raise TypeError("Not supported resolver: %s" % name)

            resolver_object.remote_start_set(self.parameters.resolver_start_remotely[name])
            resolver_object.remote_start()
            self.resolvers.append(resolver_object)

    def finalize(self):
        '''
        Stop remote resolvers and exit benchmark.
        '''
        for resolver in self.resolvers:
            if self.parameters.influxdb_upload:
                self.upload(self.parameters.result_file + '_' + resolver.name)
            resolver.stop_server()

        end_time = time.time()
        total_time = end_time - self.start_time
        m, s = divmod(total_time, 60)
        h, m = divmod(m, 60)
        log.info("> Total runtime: %d:%02d:%02d" % (h, m, s))
        # move log into results folder
        os.rename(self.parameters.logfile,
                  os.path.join(self.parameters.path_results, self.parameters.log_f))

        # kill resolvers
        sys.exit(0)

    def upload(self, file_path):
        '''
        Upload result influxDB files into remote influxDB server.

        Args:
            file_path (string): path to the uploaded file. File must have the influxdb format.
        '''
        log.info("Uploading file")
        os.system("curl -i -XPOST 'http://%s/write?db=%s&precision=s' --data-binary @%s" % (
            self.parameters.influxdb_server, self.parameters.influxdb_database, file_path))
