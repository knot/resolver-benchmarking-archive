'''
Created on Sep 21, 2016

@author: Jan Holusa
'''
import os
import logging
from six.moves import input

log = logging.getLogger()


class fileSplitter(object):
    '''
    split file into more files
    '''

    def __init__(self, parameters):
        '''
        Constructor
        '''
        self.parameters = parameters

        self.small = 0
        self.big = 0
        self.one_percentage = 0

    def splitFile(self):
        if not (self.small) or not (self.big):
            raise TypeError("Small and big value must be set using method estimate_size()")

        if not os.path.exists(self.parameters.workdir):
            q = input(
                "> Path '%s' does not exist. Shoud I create it? [yes|no]: " %
                self.parameters.workdir)
            if q == 'yes':
                os.makedirs(self.parameters.workdir)
            else:
                raise IOError("No such directory: '%s'" % (self.parameters.workdir))

        # split input file
        with open(self.parameters.infile) as infile:
            filelines = len(infile.readlines())

        one_percentage = int(round(filelines / 100))
        self.one_percentage = one_percentage
        log.info("Input file has %s lines." % str(filelines))
        log.info("One percent has %s lines." % str(one_percentage))
        log.info("Resperf needs for fill up %s lines." % str(self.small))
        log.info("Resperf needs for test %s lines. " % str(self.big))

        for i in range(0, len(self.parameters.percentage_array)):
            if int(self.parameters.percentage_array[i]) in (0, 100):
                continue

            # add x% to small file
            os.system("cat %s 2>> %s | head -n %s >%s " % (
                self.parameters.infile, self.parameters.err,
                str(self.one_percentage * int(self.parameters.percentage_array[i])),
                os.path.join(self.parameters.workdir,
                             'queryfile_' + self.parameters.percentage_array[i])))

            # fill up the small file
            log.info("Filling file: queryfile_%s with %s times original file." % (
                self.parameters.percentage_array[i],
                str(round(
                    self.small / (
                        int(self.parameters.percentage_array[i]) * self.one_percentage)))))
            print("> Filling file: queryfile_%s with %s times original file." % (
                self.parameters.percentage_array[i],
                str(round(
                    self.small / (
                        int(self.parameters.percentage_array[i]) * self.one_percentage)))))
            for j in range(0, int(round(
                self.small / (int(self.parameters.percentage_array[i]) * self.one_percentage)
            ))):
                os.system("cat %s 2>> %s | head -n %s >>%s" % (
                    self.parameters.infile, self.parameters.err,
                    str(self.one_percentage * int(self.parameters.percentage_array[i])),
                    os.path.join(self.parameters.workdir,
                                 'queryfile_' + self.parameters.percentage_array[i])))

        # #fill up the big file
        log.info("Filling file: queryfile_100 with %s times original file." %
                 str(round(self.big / filelines)))
        print("> Filling file: queryfile_100 with %s times original file." %
              str(round(self.big / filelines)))
        os.system("cat %s > %s" % (self.parameters.infile,
                                   os.path.join(self.parameters.workdir, 'queryfile_100')))

        for i in range(0, int(round(self.big / filelines))):
            os.system("cat %s >>%s" % (self.parameters.infile,
                                       os.path.join(self.parameters.workdir, 'queryfile_100')))

    def estimate_size(self, r, c, m, to_big):
        '''
        Calculate size of input files
        according the r, c, m resperf
        parameters.
        '''
        # print "R:"+str(r_sm)+"|C:"+str(c_sm)+"|M:"+str(m_sm)+"|"
        if to_big:
            self.big = (r + c) * m
        else:
            self.small = (r + c) * m
