'''
Created on Apr 20, 2017

@author: Jan Holusa
'''

import datetime
import logging
log = logging.getLogger()


class writer(object):
    '''
    Class for writing output files. Supported formats are: .js, influxdb
    '''

    def __init__(self, influxdb_path, js_path, timestemp, cnt_percentage_steps, cnt_resolvers):
        self.influxdb_file = influxdb_path
        self.js = js_path

        self.result_cache_usage_percent = 0
        self.result_resolver_name = ""
        self.result_resolver_branch = ""
        self.result_client_number = 0

        self.result_t = timestemp
        self.cnt_percentae_steps = cnt_percentage_steps
        self.percentage_counter = 0
        self.cnt_resolvers = cnt_resolvers
        self.resolver_counter = 0
        self.os = "??"
        self.deployment = "??"

        self.result_qps = 0
        self.result_sended = 0
        self.result_lost = 0
        self.result_rrcode = ""
        self.cnt_values = 0

        self.write_js_head()

    def set_branch(self, branch):
        if branch == "":
            self.result_resolver_branch = "not_set"
        else:
            self.result_resolver_branch = branch

    def set_count_of_clients(self, clients):
        '''
        Set number of clients in resperf parameter "-C"
        Args:
            clients (int): number of clients
        '''
        try:
            cli = int(clients)
        except ValueError:
            self.result_client_number = 1

        if cli > 0:
            self.result_client_number = cli
        else:
            self.result_client_number = 1

    def set_resolver_name(self, name):
        '''
        Set name of actual resolver you want to write results.
        Need to be set before you start writing results

        Args:
            name (str): name of the tested resolver.
        '''
        self.result_resolver_name = name
        self.percentage_counter = 0
        self.resolver_counter += 1
        self.write_js_before_data()

    def set_cache_usage_percent(self, percent):
        '''
        Set tested percentage of particular resolver.
        Firstly is needed to set resolver, than percentage
        '''
        self.result_cache_usage_percent = percent
        self.percentage_counter += 1

    def add_values(self, qps, sended, lost, rrcode):
        '''
        Add another velues
        Args:
            qps (float): QPS
            sended (float): count of sended queries
            lost (float): count of lost querries
            rrcode (str): return code
        '''
        self.result_qps += qps
        self.result_sended += sended
        self.result_lost += lost
        self.result_rrcode = rrcode
        self.cnt_values += 1

    def reset_values(self):
        self.result_qps = 0
        self.result_sended = 0
        self.result_lost = 0
        self.result_rrcode = ""
        self.cnt_values = 0

    def write_out(self):
        # get average
        self.result_qps /= self.cnt_values
        self.result_sended /= self.cnt_values
        self.result_lost /= self.cnt_values

        # write out
        self.write_influxdb()
        self.write_js_data()

        # set values to null
        self.reset_values()

    def write_influxdb(self):
        with open(self.influxdb_file + "_" + self.result_resolver_name, "a") as f:
            if self.result_resolver_name == "knot":
                f.write(("%s,tag_percent=%s,tag_clients=%d,tag_branch=%s percent=%s,clients=%d," +
                        "q_send=%d,lost=%d,QPS=%d,resp_codes=\"%s\" %s\n") % (
                    self.result_resolver_name,
                    self.result_cache_usage_percent + "%",
                    self.result_client_number,
                    self.result_resolver_branch,
                    self.result_cache_usage_percent,
                    self.result_client_number,
                    self.result_sended,
                    self.result_lost,
                    self.result_qps,
                    self.result_rrcode,
                    self.result_t))
            else:
                f.write(("%s,tag_percent=%s,tag_clients=%d percent=%s,clients=%d,q_send=%d," +
                        "lost=%d,QPS=%d,resp_codes=\"%s\" %s\n") % (
                    self.result_resolver_name,
                    self.result_cache_usage_percent + "%",
                    self.result_client_number,
                    self.result_cache_usage_percent,
                    self.result_client_number,
                    self.result_sended,
                    self.result_lost,
                    self.result_qps,
                    self.result_rrcode,
                    self.result_t))

    def write_js_head(self):
        '''
        It is important to find OS using get_os before
        you run this function

        See: get_os()
        '''
        log.info("Creating .js file: %s" % self.js)
        with open(self.js, "w") as out:
            out.write("var chart = {\n")
            out.write("\t'type': 'QPS / cache usage',\n")
            out.write("\t'date': '%s',\n" % datetime.datetime.fromtimestamp(
                int(self.result_t)).strftime('%Y-%m-%d'))  # TODO otestovat
            out.write("\t'properties': {\n")

            out.write("\t\t'os': '%s',\n" % self.os)
            out.write("\t\t'deployment': '%s'\n" % self.deployment)
            out.write("\t},\n")

            out.write("\t'series': [\n")

    def write_js_before_data(self):
        with open(self.js, "a") as f:
            f.write("{ name: '%s', data: [" % self.result_resolver_name)

    def write_js_data(self):
        '''
        Write out javascript data.
        '''
        with open(self.js, "a") as f:
            f.write("[%s,%s]" % (self.result_cache_usage_percent, self.result_qps))
            if self.percentage_counter <= self.cnt_percentae_steps:
                f.write(", ")
            else:
                self.write_js_after_data()

    def write_js_after_data(self):
        with open(self.js, "a") as f:
            f.write("]}\n")
            if self.resolver_counter <= self.cnt_resolvers:
                f.write(",\n")
            else:
                self.write_js_tail()

    def write_js_tail(self):
        '''
        Write out javascript tail. Called after data.
        '''
        with open(self.js, "a") as out:
            out.write("]};\n\n")
            out.write("KNOT.rawChartData.push(chart);\n")
#     def output(f, sended, qps, lost, cc, rrcode, timestemp):
#         f.write("%.3f\t%.3f\t%.3f\t%.3f\t%s\t%d\n" % (cc, qps, sended, lost, rrcode, timestemp))
