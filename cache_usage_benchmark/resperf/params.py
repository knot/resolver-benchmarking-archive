'''
Created on Apr 21, 2017

@author: Jan Holusa
'''

import ConfigParser
from ConfigParser import NoOptionError
from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter
import logging
import os
import sys

import local_constants as C

log = logging.getLogger()


class params(object):
    '''
    Class handeling all parameters of
    cache usage benchmark.
    '''

    def __init__(self, timestemp):
        self.infile = ""
        self.iteration = 1

        self.curdir, tail = os.path.split(os.path.dirname((os.path.abspath(__file__))))
        self.workdir = os.path.join(self.curdir, "workplace")
        self.data = os.path.join(self.workdir, "file")
        self.err = os.path.join(self.workdir, "err")
        self.path_results = os.path.join(self.curdir, "results", str(timestemp)[:10])
        self.timestemp = str(timestemp)[:10]
        self.result_file = os.path.join(self.path_results, "result_resolver")
        self.local_interface = "eth0"

        if not os.path.exists(self.path_results):
            os.makedirs(self.path_results)
            log.warning("Result directory does not exist. Creating: " + self.path_results)

        self.log_f = "benchmark.log"
        self.logfile = os.path.join(self.curdir, self.log_f)

        # Parameters for resperf
        self.pre_resperf_param = "-r 0 -c 3 -t 12 -m 100000"
        self.resperf_param = "-r 3 -c 0 -m 200000"

        # Parameters for testing
        self.resolver_port = {"knot": 50003, "bind": 50002, "unbound": 50001, "pdns": 50005}
        self.resolver_ip = {
            "knot": "172.20.20.160",
            "bind": "172.20.20.160",
            "unbound": "172.20.20.160",
            "pdns": "172.20.20.160"}
        self.resolver_start_remotely = {
            "knot": False,
            "bind": False,
            "unbound": False,
            "pdns": False}
        self.percentage_array = ['50']

        self.knot_branch = "master"
        self.knot_service_port = 50004
        self.out_js = os.path.join(self.path_results, "resolver.js")

        # ip of DNS server running resolvers
        # ip_server = '172.20.20.160'
        self.influxdb_server = '217.31.192.164:8086'
        self.influxdb_upload = True
        self.influxdb_database = 'resolvers'

    def conf_parse(self, configFile):
        if os.path.isfile(configFile):
            cp = ConfigParser.ConfigParser()
            cp.read(configFile)

            try:
                param = cp.get('general', 'workdir')
            except NoOptionError:
                log.info("Workdir not set. Default is " + self.workdir)
            else:
                self.workdir = os.path.join(self.curdir, param)
                log.debug("Workdir not set. Value is " + self.workdir)

            if not os.path.exists(self.workdir):
                os.makedirs(self.workdir)
                log.warning("Workdir directory does not exist. Creating: " + self.workdir)

            try:
                param = cp.get('general', 'iteration')
            except NoOptionError:
                log.info("Iteration number not set. Default is " + str(self.iteration))
            else:
                self.iteration = int(param)
                log.debug("Iteration number set. Value is " + str(self.iteration))

            try:
                param = cp.get('general', 'influxdb_server')
            except NoOptionError:
                log.info("InfluxDB server not set. Default is " + self.influxdb_server)
            else:
                self.influxdb_server = param
                log.debug("InfluxDB server set. Value is " + self.influxdb_server)

            try:
                param = cp.get('general', 'influxdb_upload')
            except NoOptionError:
                log.info(
                    "InfluxDB upload flag not set. Default is " +
                    "yes" if self.influxdb_upload else "no")
            else:
                self.influxdb_upload = True if param == "yes" else False
                log.debug(
                    "InfluxDB upload flag set. Value is " +
                    "yes" if self.influxdb_upload else "no")

            try:
                param = cp.get('general', 'influxdb_database')
            except NoOptionError:
                log.info("InfluxDB database not set. Default is " + self.influxdb_database)
            else:
                self.influxdb_database = param

            try:
                param = cp.get('general', 'load_resperf_param')
            except NoOptionError:
                log.info("Load resperf param not set. Default is " + self.pre_resperf_param)
            else:
                self.pre_resperf_param = param
                log.debug("Load resperf param set. Value is " + self.pre_resperf_param)

            try:
                param = cp.get('general', 'measure_resperf_param')
            except NoOptionError:
                log.info("Measure resperf param not set. Default is " + self.resperf_param)
            else:
                self.resperf_param = param
                log.debug("Measure resperf param set. Value is " + self.resperf_param)

            # Local interface name
            try:
                param = cp.get('general', 'local_interface')
            except NoOptionError:
                log.info("Local interface not set. Default is " + self.local_interface)
            else:
                self.local_interface = param
                log.debug("Local interface set: " + self.local_interface)

            # Percentages
            try:
                param = cp.get('general', 'percentages')
            except NoOptionError:
                log.info("Percentages are not set. Default is " + str(self.percentage_array))
            else:
                self.percentage_array = param.split(",")
                for i in range(0, len(self.percentage_array)):
                    self.percentage_array[i] = self.percentage_array[i].strip()
                log.debug("Percentages are set: " + str(self.percentage_array))

            # RESOLVERS CONFIG
            cp_dict = dict(self.resolver_ip)
            for key in cp_dict:
                try:
                    param = cp.get(key, 'port')
                except Exception as e:
                    log.info("%s not set. It is not going to be tested" % key)
                    del self.resolver_ip[key]
                    del self.resolver_port[key]
                    continue
                else:
                    self.resolver_port[key] = int(param)
                    log.debug("%s port set: %d" % (key, self.resolver_port[key]))

                try:
                    param = cp.get(key, 'ip')
                except Exception as e:
                    log.info("%s not set. It is not going to be tested" % key)
                    del self.resolver_ip[key]
                    del self.resolver_port[key]
                    continue
                else:
                    self.resolver_ip[key] = param
                    log.debug("%s ip set: %s" % (key, self.resolver_ip[key]))

                try:
                    param = cp.get(key, 'start_remotely')
                except Exception as e:
                    log.debug("Resolver %s is not going to start remotely." % key)
                else:
                    self.resolver_start_remotely[key] = True if param == "yes" else False
                    if self.resolver_start_remotely[key]:
                        log.info("Start resolver %s remotely set!" % key)

                if key == "knot":
                    try:
                        param = cp.get(key, 'branch')
                    except Exception as e:
                        log.info("%s branch not set. Default is master." % key)
                    else:
                        self.knot_branch = param
                        log.debug("%s branch set to '%s'." % (key, self.knot_branch))

                    try:
                        param = cp.get(key, 'service_port')
                    except Exception as e:
                        log.info("%s service port not set. Default is %s." %
                                 (key, str(self.knot_service_port)))
                    else:
                        self.knot_service_port = param
                        log.debug("%s service port set to '%s'." %
                                  (key, str(self.knot_service_port)))

                        # end for key (knot, bind, unbound...
        else:
            raise IOError("File '" + configFile + "' not found.")

    def read_params(self, argv=None):
        if argv is None:
            argv = sys.argv
        else:
            sys.argv.extend(argv)

        program_name = os.path.basename(sys.argv[0])
        program_version = "v%s" % C.__version__
        program_build_date = str(C.__updated__)
        program_version_message = '%%(prog)s %s (%s)' % (program_version, program_build_date)
        program_shortdesc = __import__('__main__').__doc__.split("\n")[1]
        program_license = '''%s

      Created by Jan Holusa on %s.
      Copyright 2017 nic.cz. All rights reserved.

    USAGE
    ''' % (program_shortdesc, str(C.__date__))

        try:
            # Setup argument parse_result
            parse_result = ArgumentParser(
                description=program_license,
                formatter_class=RawDescriptionHelpFormatter)
            parse_result.add_argument(
                "-c", "--config", dest="config", help="load config file", metavar="file")
            parse_result.add_argument(
                "-i", "--input", dest="input", help="load input dataset file", metavar="file")
            parse_result.add_argument(
                '-V', '--version', action='version', version=program_version_message)
            parse_result.add_argument(
                '-d', '--debug', dest='debug', help="Turn on debug outputs to stdout.",
                action='store_const', const=True, default=False)

            # Process arguments
            args = parse_result.parse_args()

            config = args.config
            input_file = args.input

            if config:
                if os.path.isfile(config):
                    self.conf_parse(config)

                else:
                    raise IOError("File '" + config + "' not found.")
            else:
                log.error("Missing config file.")
                raise IOError("Missing config file.")

            if input_file:
                if os.path.isfile(input_file):
                    self.infile = input_file
                    log.debug("Input file is: " + self.infile)
                else:
                    raise IOError("File '" + input_file + "' not found.")
            else:
                log.info("Input file not set. Default is " + self.infile)

        except KeyboardInterrupt:
            # handle keyboard interrupt
            return 0
        except Exception as e:
            indent = len(program_name) * " "
            sys.stderr.write(program_name + ": " + repr(e) + "\n")
            sys.stderr.write(indent + "  for help use --help")
            return 2

    @staticmethod
    def parse_params(params):
        '''
        Parse params of resperf and get r c m C
        values.
        '''
        r = "-r"
        c = "-c"
        m = "-m"
        C = "-C"
        space = " "

        r_sm_start = params.find(r)
        if r_sm_start == -1:
            r_sm = 60  # default resperf -r value
        else:
            r_sm_start += len(r)
            r_sm = params[r_sm_start:].strip()
            r_sm_end = r_sm.find(space)
            if r_sm_end != -1:
                r_sm = r_sm[:r_sm_end]

        c_sm_start = params.find(c)
        if c_sm_start == -1:
            c_sm = 0  # default resperf -c value
        else:
            c_sm_start += len(c)
            c_sm = params[c_sm_start:].strip()
            c_sm_end = c_sm.find(space)
            if c_sm_end != -1:
                c_sm = c_sm[:c_sm_end]

        m_sm_start = params.find(m)
        if m_sm_start == -1:
            m_sm = 100000  # default resperf -m value
        else:
            m_sm_start += len(m)
            m_sm = params[m_sm_start:].strip()
            m_sm_end = m_sm.find(space)
            if m_sm_end != -1:
                m_sm = m_sm[:m_sm_end]

        C_sm_start = params.find(C)
        if C_sm_start == -1:
            C_sm = 1  # default resperf -m value
        else:
            C_sm_start += len(C)
            C_sm = params[C_sm_start:].strip()
            C_sm_end = C_sm.find(space)
            if C_sm_end != -1:
                C_sm = C_sm[:C_sm_end]

        log.debug("r:" + str(r_sm) + " c:" + str(c_sm) + " m:" + str(m_sm) + " C:" + str(C_sm))

        return int(r_sm), int(c_sm), int(m_sm), int(C_sm)
