#!/usr/local/bin/python2.7
# encoding: utf-8
'''
@author:     Jan Holusa

@copyright:  2017 nic.cz. All rights reserved.

@contact:    jan.holusa@nic.cz
'''

import sys
import handler

if __name__ == "__main__":
    my_handler = handler.handler()
    my_handler.start()
    sys.exit(0)
