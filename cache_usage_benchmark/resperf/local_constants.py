#!/usr/bin/env python
# encoding: utf-8

'''
Created on Mar 22, 2017

@author: Jan Holusa
'''

import os

__all__ = []
__version__ = 1.0
__date__ = '2016-10-17'
__updated__ = '2017-4-30'


head, tail = os.path.split(os.path.dirname(os.path.abspath(__file__)))
SCRIPT_FOLDER = os.path.join(head, "scripts")
RESOLVER_CONFIG_FOLDER = os.path.join(head, "resolvers_setup")
LOG = "benchmark.log"
