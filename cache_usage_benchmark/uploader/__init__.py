

import upload
import sys

# python2.7 __init__.py results/11100121221 1

if len(sys.argv) < 3:
    raise ValueError(
        "Must have 2 argument." +
        " The workdir and the option (1-send whole workdir, file - send just file.")

up = upload.upload(sys.argv[1])

if sys.argv[2] == '1':
    up.upload_workdir()
else:
    if len(sys.argv) != 4:
        raise ValueError("file argument needs DB.")
    up.upload_file(sys.argv[2], sys.argv[3])
