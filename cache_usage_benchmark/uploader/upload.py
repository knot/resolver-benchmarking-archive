'''
Created on Sep 29, 2016

@author: Jan Holusa
'''
import os


class upload(object):
    '''
    Upload data to influxDB
    '''

    def __init__(self, workdir):
        '''
        Constructor
        '''
        self.workdir = workdir

    def upload_workdir(self):
        '''
        Uploads all files in workdir.
        '''
        print("Uploading whole workdir: " + self.workdir)
        knot = os.path.join(self.workdir, "result_resolver_knot")
        bind = os.path.join(self.workdir, "result_resolver_bind")
        unbound = os.path.join(self.workdir, "result_resolver_unbound")
        os.system(
            "curl -i -XPOST 'http://217.31.192.164:8086/write?db=resolvers&precision=s' " +
            "--data-binary @" + knot)
        os.system(
            "curl -i -XPOST 'http://217.31.192.164:8086/write?db=resolvers&precision=s' " +
            " --data-binary @" + bind)
        os.system(
            "curl -i -XPOST 'http://217.31.192.164:8086/write?db=resolvers&precision=s' " +
            "--data-binary @" + unbound)
        print("Done")

    def upload_file(self, f, db):
        '''
        Uploads one file.
        '''
        print("Uploading file: " + os.path.join(self.workdir, f))
        path_to_file = os.path.join(self.workdir, f)
        os.system("curl -i -XPOST 'http://localhost:8090/write?db=" +
                  db + "&precision=s' --data-binary @" + path_to_file)
        print("Done")
