#!/bin/bash

#export CFLAGS="-DNDEBUG"
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
LOG="stdout.log"
BASEDIR=$(dirname "$0") 

#Add ssh key to access to machine with dns resolvers 
#eval `ssh-agent -s`
#ssh-add /root/.ssh/id_benchmark

#check git repository
git -C $BASEDIR pull

#run the python benchark for 100clients
python2.7 $BASEDIR/resperf -c $BASEDIR/config/cache_usage_c100.cfg -i $BASEDIR/data/queryfile_uniq_500k &>>$BASEDIR/$LOG


#remove ssh key
#ssh-add -d /root/.ssh/id_benchmark
