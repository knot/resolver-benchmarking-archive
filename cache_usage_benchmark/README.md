Please note that cache usage benchmark is unmaintained at the moment!


# Overview and usage
This benchmark is used for comparing querries per second (QPS) depending 
on actual cache usage. It is using 'resperf' program for measuring. 
Results
and methodics can be found at 
[knot-resolver](https://gitlab.labs.nic.cz/knot/resolver/wikis/Comparison-different-cache-usage-and-QPS) 
wiki page.

## What it roughly does
Program fill up the cache of tested resolver to required amount (10%, 
20%, ...) and then sends querries to resolver and make a measurement. 
For filling up and for measurement is used *resperf*.

## What do you need
On the test machine:
* Python2.7
* [Resperf](http://nominum.com/measurement-tools/)

On the machine with servers:
* Bind, Unbound and Knot-resolver (examples of config files is in folder *resolv.setup*). 

For now daemons are runned with this options:
* Bind -u bind -n 1 
* kresd --config=kresd.conf --forks=1 /rundir/
* unbound (without any special options)
* pdns_recursor (without any special options)

## How to run tests
* Test can be run by command `#python2.7 run -c config.cfg -i dataset`

### Automatization
It is possible to schedule benchmark to run automatically at particular hour.
Example of configuration is in `starter.sh` script. This script is also
using ansible-playbook `server-check.yml` which is checking if resolvers 
are running and if they are in the latest version.

## Configuration file
Contains `general` section and one section for each resolver. Supported resolvers
are for now `[knot]`, `[bind]`, `[unbound]` and `[pdns]`. In section general
you can configure these parameters:
* `workdir` which indicates directory for program use. This directory
will be cleaned after program finished.
* `iteration` sets the number of iterations.
* `load_resperf_param` indicates parameters for resperf program to fill 
up cache. 
(it is also called test A in code)
* `measure_resperf_param` indicates parameters for resperf program to 
perform measurement.
(it is also called test B in code)
* `influxdb_server` indicates ip address of influxDB server and port.
* `influxdb_upload` indication flag. To upload or not to upload?
* `influxdb_database` indicates name of influxDB database where data 
will be uploaded.
* `local_interface` indicates which outgoing interface should be used.
* `percentages` indicates array of percentages of filled cache to be tested.
 
 In resolver sections, you can configure these obligatory parameters:
* `port` indicates port numbers at which resolvers are running. Must be
used with `resolvers` parameter.
* `ip` indicates IP addres, where resolver will be started or where resolver is running.
* `start_remotely` is flag, which indicates start of the server in given ip and port.
Possible values are yes and no. Default is no.

In `[knot]` resolver section, there are two more parameters:
* `branch` sets name of branch to be tested. Default is master.
* `service_port` sets the service port for socat. Default is 50004.

Example of config file: `config/example.cfg`

# Method
Kresd, Named, Unbound and PowerDNS recursor (in the latest available Ubuntu package version) has been started on one server using different port numbers and test script was started on another server. Test script were send 
querries to it using *resperf*. Amount of querries depends on the amount of the cache usage we are measuring (I am measuring 30% usage means that I am going
to send 30% of the total amount of querries). This first step is made in loop until there is the smallest possible loss of packets at the point of the maximum throughput.
The smallest possible loss of packets means that there is a threshold set to 0% loss at the start. If the test don't reach the threshold after 5 times repetition, it increase 
the threshold of 5%. 
The moment in which threshold satisfy is taken as a signification that cache is x% full.

Querries per second (QPS) value is obtained from the *resperf* output by sending all querries to resolver after first reduced amount of querries.

We are using these parameters to run resperf, where first row is used to fill up the cache and second row is used to perform measurement.
```
1] -r 0 -c 3 -t 12 -m 100000
2] -r 3 -c 0 -m 200000
```

# Data
Input data file is sample file from [Nominum webpage](http://nominum.com/measurement-tools/) reduced 100k queries.

Input data file is splitted into 20 files. First 10 files are made according the percentage of all queries (queryfile_10 contains first 10% of all queries and so on). These x% quieries are
repeated in the file. Second 10 files are made from whole input file merged with percentage file (queryfile_100 is made from input file merged with queryfile_10). It is 
 because resperf needs enough input querries. This repetition should not have influence to results. How it is merged is shown below.
 
```
Original input file: |Original|
queryfile_10: 		 |10file|
queryfile_100: 		 |Original|10file|10file|...
```

Examples of data files are in `data` folder.
