#!/bin/bash

export CFLAGS="-DNDEBUG"
#local path for crontab
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games"
LOG="stdout.log"
BASEDIR=$(dirname "$0")  

#add ssh key to the foreign server
#eval `ssh-agent -s`
#ssh-add /root/.ssh/id_benchmark                                                

#check git repository
git -C $BASEDIR pull

#run python benchmark
python2.7 $BASEDIR/resperf -c $BASEDIR/config/cache_usage.cfg -i $BASEDIR/data/queryfile_uniq_100k &>>$BASEDIR/$LOG

#remove ssh key to the foreign server
#ssh-add -d /root/.ssh/id_benchmark
