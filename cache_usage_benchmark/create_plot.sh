#!/bin/sh
#
# Script to generate graphs borowed from resperf program and edited 
# http://www.nominum.com/measurement-tools/
#

# Program locations - change these if not in $PATH
gnuplot=gnuplot

# The gnuplot terminal type.  This determines the image format for the
# plots; "png" or "gif" will both work as long as the corresponding
# terminal support is compiled into your copy of gnuplot.
terminal=png

# Create a unique ID for this report
id=$1

# Set up file names
reportfile="$id.html"
plotfile="$id.gnuplot"
rate_graph="$id.rate.$terminal"
latency_graph="$id.latency.$terminal"

# Create plots

if
    $gnuplot <<EOF
set terminal $terminal
set output "$rate_graph"
set title "Query / response / failure rate"
set key top left
set xlabel "Time (seconds)"
set yrange [0:]
plot \
"$plotfile" using 1:3 title "Queries sent per second" with lines, \
"$plotfile" using 1:4 title "Total responses received per second" with lines, \
"$plotfile" using 1:5 title "Failure responses received per second" with lines
EOF
then
    :
else
    echo "`basename $0`: error running gnuplot" >&2; exit 1;
fi

if
    $gnuplot <<EOF
set terminal $terminal
set output "$latency_graph"
set title "Latency"
set key top left
set xlabel "Time (seconds)"
set yrange [0:]
plot \
"$plotfile" using 1:6 title "Average latency (seconds)" with lines
EOF
then
    :
else
    echo "`basename $0`: error running gnuplot" >&2; exit 1;
fi




